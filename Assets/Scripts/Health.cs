﻿using UnityEngine;

/// <summary>
/// Controls the amount of health that this game object has.
/// </summary>
public class Health : MonoBehaviour
{
    /// <summary>
    /// The current number of health points.
    /// </summary>
    public int hp = 3;

    /// <summary>
    /// The maximum and minimum number of health points. These variables are
    /// public but hidden from the editor. Values are set on Start.
    /// </summary>
    [HideInInspector]
    public int maxHp;
    [HideInInspector]
    public int minHp;

    /// <summary>
    /// Called on the frame when a script is enabled just before any of
    /// the Update methods are called the first time.
    /// Reference: https://docs.unity3d.com/ScriptReference/MonoBehaviour.Start.html
    /// </summary>
    void Start ()
    {
        maxHp = hp;
        minHp = 0;
    }

    /// <summary>
    /// Called once per frame. Makes sure that hp spends very little time below or above minHp and maxHp.
    /// Reference: https://docs.unity3d.com/ScriptReference/MonoBehaviour.Update.html
    /// </summary>
    void Update ()
    {
        if (hp < minHp)
        {
            hp = 0;
        }
        else if (hp > maxHp)
        {
            hp = maxHp;
        }
    }

    /// <summary>
    /// Applies damage to the health points. The object is destroyed by default
    /// when hp is at or below minHp; modify this to fit your needs.
    /// </summary>
    /// <param name="damage">the amount of damage</param>
    public void Damage (int damage)
    {
        hp -= damage;

        if (hp <= minHp)
        {
            Destroy (this.gameObject);
        }
    }
}