﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// HealthBar that displays health nodes based on the Health component
/// in this game object. A health node displays whether a health point is
/// empty or full. This HealthBar works best for a game where one of the main
/// objectives is "to not get hit", since each health node is designed to be
/// detached from one another. Examples are the vanilla healthbars in games such as
/// Minecraft and in SHMUPs.
/// </summary>
[RequireComponent (typeof(Health))]
public class HealthBar : MonoBehaviour
{
    /// <summary>
    /// Displays whether a node contains health.
    /// </summary>
    public class HealthNode
    {
        public GameObject healthNodeImageEmpty;
        public GameObject healthNodeImageFull;
    }

    /// <summary>
    /// The name of the canvas that this healthbar will be a child of.
    /// </summary>
    const string healthBarCanvasName = "HealthBarCanvas";

    /// <summary>
    /// The position of the healthbar. This can be adjusted in the editor through
    /// adjusting coordinates manually or by moving the handle around in the editor GUI.
    /// </summary>
    public Vector2 pos;

    /// <summary>
    /// Prefabs containing the sprites of full and empty health nodes.
    /// </summary>
    public GameObject healthNodeImageFullPrefab;
    public GameObject healthNodeImageEmptyPrefab;

    /// <summary>
    /// The camera that this health bar will display in.
    /// </summary>
    public Camera worldCamera;

    /// <summary>
    /// The array representation of the healthbar.
    /// </summary>
    private HealthNode[] healthNodes;
    private int healthNodeCursor;

    /// <summary>
    /// Called on the frame when a script is enabled just before any of
    /// the Update methods are called the first time.
    /// Reference: https://docs.unity3d.com/ScriptReference/MonoBehaviour.Start.html
    /// </summary>
    void Start ()
    {
        if (DependentGameObjectsAreSet ()) 
        {
            GameObject healthBarCanvas = InitializeHealthBarCanvas ();
            InitializeHealthNodesArray (healthBarCanvas);
        } 
        else 
        {
            Debug.Log ("The healthBar prefab(s) or the world camera are not assigned in the editor");
        }
    }

    /// <summary>
    /// Called once per frame.
    /// Reference: https://docs.unity3d.com/ScriptReference/MonoBehaviour.Update.html
    /// </summary>
    void Update ()
    {
        if (DependentGameObjectsAreSet ())
        {
            UpdateHealthNodes ();
        }
    }

    /// <summary>
    /// Called when this object will be destroyed.
    /// Reference: https://docs.unity3d.com/ScriptReference/MonoBehaviour.OnDestroy.html
    /// </summary>
    void OnDestroy ()
    {
        if (DependentGameObjectsAreSet ())
        {
            UpdateHealthNodes ();
        }
    }

    /// <summary>
    /// Checks whether the dependent game objects are set in the editor.
    /// </summary>
    /// <returns>True if the dependent game objects are set</returns>
    private bool DependentGameObjectsAreSet()
    {
        if (healthNodeImageFullPrefab && healthNodeImageEmptyPrefab && worldCamera)
        {
            return true;
        }
        return false;
    }

    /// <summary>
    /// Initializes the health bar canvas.
    /// </summary>
    /// <returns>GameObject healthBarCanvas - the canvas that this health bar will be a child of</returns>
    private GameObject InitializeHealthBarCanvas ()
    {
        GameObject healthBarCanvas = new GameObject(healthBarCanvasName, typeof(RectTransform));
        healthBarCanvas.GetComponent<RectTransform>().position = Vector3.zero;
        healthBarCanvas.AddComponent<Canvas>();
        healthBarCanvas.GetComponent<Canvas>().renderMode = RenderMode.ScreenSpaceCamera;
        healthBarCanvas.GetComponent<Canvas>().worldCamera = worldCamera.GetComponent<Camera>();
        healthBarCanvas.AddComponent<CanvasScaler>();
        healthBarCanvas.AddComponent<GraphicRaycaster>();
        return healthBarCanvas;
    }

    /// <summary>
    /// Initializes the health nodes array.
    /// </summary>
    /// <param name="healthBarCanvas">the canvas that this health bar will be a child of.</param>
    private void InitializeHealthNodesArray (GameObject healthBarCanvas)
    {
        Health health = GetComponent<Health>();
        healthNodes = new HealthNode[health.hp];
        healthNodeCursor = healthNodes.Length - 1;

        for (int i = 0; i < health.hp; i++)
        {
            GameObject healthpointImageEmpty = Instantiate (healthNodeImageEmptyPrefab, new Vector3 (pos.x + i * healthNodeImageEmptyPrefab.transform.localScale.x, pos.y, 0), Quaternion.identity) as GameObject;
            GameObject healthpointImageFull = Instantiate (healthNodeImageFullPrefab, new Vector3 (pos.x + i * healthNodeImageFullPrefab.transform.localScale.x, pos.y, 0), Quaternion.identity) as GameObject;

            healthpointImageEmpty.transform.SetParent(healthBarCanvas.transform);
            healthpointImageFull.transform.SetParent(healthBarCanvas.transform);

            HealthNode healthNode = new HealthNode();
            healthNode.healthNodeImageEmpty = healthpointImageEmpty;
            healthNode.healthNodeImageFull = healthpointImageFull;

            healthNode.healthNodeImageEmpty.gameObject.SetActive(false);

            healthNodes[i] = healthNode;
        }
    }

    /// <summary>
    /// Updates the health nodes array to reflect the correct number
    /// of health points in the Health component in this game object.
    /// </summary>
    private void UpdateHealthNodes ()
    {
        Health health = GetComponent<Health> ();

        // healthpointsCursor to match the player's health
        if (healthNodeCursor > health.hp - 1)
        {
            while (healthNodeCursor != health.hp - 1 && health.hp >= health.minHp)
            {
                healthNodes [healthNodeCursor].healthNodeImageEmpty.SetActive (true);
                healthNodes [healthNodeCursor].healthNodeImageFull.SetActive (false);
                healthNodeCursor--;
            }
        } 
        else if (healthNodeCursor < health.hp - 1)
        {
            while (healthNodeCursor != health.hp - 1 && health.hp <= health.maxHp)
            {
                healthNodes [healthNodeCursor + 1].healthNodeImageFull.SetActive (true);
                healthNodes [healthNodeCursor + 1].healthNodeImageEmpty.SetActive (false);
                healthNodeCursor++;
            }
        }
    }
}