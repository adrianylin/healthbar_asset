﻿using UnityEngine;
using UnityEditor;

/// <summary>
/// Custom editor for HealthBar.
/// </summary>
[CustomEditor (typeof(HealthBar))]
public class HealthBarEditor : Editor
{
    /// <summary>
    /// Enables the editor to handle an event in the scene view. Contains helpers in positioning healthbars.
    /// Reference: https://docs.unity3d.com/ScriptReference/Editor.OnSceneGUI.html
    /// </summary>
    void OnSceneGUI ()
    {
        HealthBar healthBar = target as HealthBar;
        float width = 0.5f;
        float height = 0.5f;

        Handles.DrawSolidRectangleWithOutline(new Rect((healthBar.pos.x - width / 2), healthBar.pos.y - height / 2, width, height), Color.red, Color.gray);
        Handles.Label(new Vector3(healthBar.pos.x, healthBar.pos.y - height / 2), "HealthBar Position");
        EditorGUI.BeginChangeCheck();
        Quaternion rotation = Quaternion.identity;
        Vector3 position = Handles.PositionHandle(new Vector3(healthBar.pos.x, healthBar.pos.y, 0), rotation);
        if (EditorGUI.EndChangeCheck())
        {
            Undo.RecordObject(healthBar, "Moved HealthBar");
            healthBar.pos.x = position.x;
            healthBar.pos.y = position.y;
        }
    }
}
